import 'dart:convert';

import 'package:bitcoin_ticker/coin_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = "USD";
  CoinData cd = CoinData();
  List<String> cryptValues = ["?", "?", "?"];

  DropdownButton<String> getDropdownButton() {
    List<DropdownMenuItem<String>> items = [];
    for (String item in currenciesList) {
      items.add(DropdownMenuItem(
        child: Text(item),
        value: item,
      ));
    }
    return DropdownButton<String>(
      value: selectedCurrency,
      items: items,
      onChanged: (value) async {
        List<String> tempoList = ["?", "?", "?"];

        for (int i = 0; i < cryptoList.length; i++) {
          dynamic temp = await cd.getCoinJson(cryptoList[i], value);
          double temp2 = temp;
          tempoList[i] = temp2.toStringAsFixed(0);
        }
        setState(() {
          selectedCurrency = value;
          cryptValues = tempoList;
        });
      },
    );
  }

  CupertinoPicker getDropdownItemIOS() {
    List<Text> items = [];
    for (String item in currenciesList) {
      items.add(Text(item));
    }
    return CupertinoPicker(
      itemExtent: 32,
      backgroundColor: Colors.lightBlue,
      onSelectedItemChanged: (selectedIndex) {
        setState(() {});
      },
      children: items,
    );
  }

  Widget chooseButton() {
    if (Platform.isAndroid) {
      return getDropdownButton();
    } else {
      return getDropdownItemIOS();
    }
  }

  // List<Padding> driverWidget(){
  //   List<Padding> temp;
  //   // for(int i=0;i<cryptValues.length;i++){
  //   //   print(cryptoList[i]+" "+cryptValues[i]);
  //   //   Widget temp2=widgetCoin(cryptoList[i], cryptValues[i]);
  //   //   temp.add(temp2);
  //   // }
  //   // print(temp);
  //   //temp.add(Instance of );
  //   temp.add(widgetCoin(cryptoList[0],cryptValues[0]));
  //   return temp;
  // }

  Widget widgetCoin(String coin, String value) {
    return Padding(
      padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 $coin = $value $selectedCurrency',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          // driverWidget(),
          Column(children: <Widget>[
            widgetCoin(cryptoList[0], cryptValues[0]),
            widgetCoin(cryptoList[1], cryptValues[1]),
            widgetCoin(cryptoList[2], cryptValues[2]),
          ]),
          Container(
              height: 150.0,
              alignment: Alignment.center,
              padding: EdgeInsets.only(bottom: 30.0),
              color: Colors.lightBlue,
              child: chooseButton())
        ],
      ),
    );
  }
}
