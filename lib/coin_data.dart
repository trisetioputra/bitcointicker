import 'dart:convert';

import 'package:http/http.dart' as http;
const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

class CoinData {
  final apikey="C2428A8E-EC92-4B42-89B5-F85C8531085D";
  final mapURL="https://rest.coinapi.io/v1/exchangerate";

  Future getData(String url) async{
    http.Response resp= await http.get(Uri.parse(url));
    var decodes= jsonDecode(resp.body);
    return decodes;
  }

  Future<dynamic> getCoinJson(String coin,String currency) async{
    var data= await getData("$mapURL/$coin/$currency?apikey=$apikey");

    // var as=weatherData["weather"][0]["Clear"];
    return data["rate"];
  }



}
